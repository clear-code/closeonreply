/*
 * For every function:
 * MsgReplySender
 * MsgReplyGroup
 * MsgReplyToAllMessage
 * MsgReplyToListMessage
 *
 * Every time we are redefining the function
 * we call the original function then we choose if we should close the window or the tab or do nothing
 */

const CloseOnReply = {
  COMPOSE_WINDOW_READY: 'closeonreply:composeWindowReady',

  init() {
    const originalMsgReplySender = MsgReplySender;
    window.MsgReplySender = function(event) {
      CloseOnReply.readyToClose();
      return originalMsgReplySender(event);
    };

    const originalMsgReplyGroup = MsgReplyGroup;
    window.MsgReplyGroup = function(event) {
      CloseOnReply.readyToClose();
      return originalMsgReplyGroup(event);
    };

    const originalMsgReplyToAllMessage = MsgReplyToAllMessage;
    window.MsgReplyToAllMessage = function(event) {
      CloseOnReply.readyToClose();
      return originalMsgReplyToAllMessage(event);
    };

    const originalMsgReplyToListMessage = MsgReplyToListMessage;
    window.MsgReplyToListMessage = function(event) {
      originalMsgReplyToListMessage(event);
      CloseOnReply.readyToClose();
    };

    const originalMsgForwardMessage = MsgForwardMessage;
    window.MsgForwardMessage = function(event) {
      CloseOnReply.readyToClose();
      return originalMsgForwardMessage(event);
    };


    const originalMsgForwardAsInline = MsgForwardAsInline;
    window.MsgForwardAsInline =function(event) {
      CloseOnReply.readyToClose();
      return originalMsgForwardAsInline(event);
    };

    const originalMsgForwardAsAttachment = MsgForwardAsAttachment;
    window.MsgForwardAsAttachment =function(event) {
      CloseOnReply.readyToClose();
      return originalMsgForwardAsAttachment(event);
    };

    const { Services } = Components.utils.import('resource://gre/modules/Services.jsm');
    Services.obs.addObserver(this, this.COMPOSE_WINDOW_READY, false);
    const messagepane = document.getElementById('messagepane');
    if (messagepane) {
      this.onMessageLoad = this.onMessageLoad.bind(this);
      messagepane.addEventListener('load', this.onMessageLoad, true);
    }
    window.addEventListener('unload', this.destroy.bind(this), { once: true });
  },
  destroy() {
    const { Services } = Components.utils.import('resource://gre/modules/Services.jsm');
    Services.obs.removeObserver(this, this.COMPOSE_WINDOW_READY);
    const messagepane = document.getElementById('messagepane');
    if (messagepane)
      messagepane.removeEventListener('load', this.onMessageLoad, true);
  },

  onMessageLoad() {
    this.waitingForComposition = false;
  },

  readyToClose() {
    this.waitingForComposition = true;
  },

  observe(subject, topic, data) {
    switch (topic) {
      case this.COMPOSE_WINDOW_READY:
        if (this.waitingForComposition) {
          this.waitingForComposition = false;
          this.tryClose();
        }
        break;
    }
  },

  tryClose() {
    const w = document.getElementById('messengerWindow');
    const { Services } = Components.utils.import('resource://gre/modules/Services.jsm');
    const prefs = Services.prefs.getBranch('mail.');
    if (w.baseURI != 'chrome://messenger/content/messenger.xul') {
      window.close();
    }
    else if (prefs.getIntPref('openMessageBehavior')== 2) {
      const tabmail = document.getElementById('tabmail');
      tabmail.removeCurrentTab();
    }
  }
};
CloseOnReply.init();
