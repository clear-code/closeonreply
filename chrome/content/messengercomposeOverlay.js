/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

const CloseOnReplyHelper = {
  init() {
    const originalSendMessage = SendMessage;
    window.SendMessage = function(...args) {
      originalSendMessage(...args);
      CloseOnReplyHelper.onCompositionFinish();
    };

    const originalSendMessageWithCheck = SendMessageWithCheck;
    window.SendMessageWithCheck = function(...args) {
      originalSendMessageWithCheck(...args);
      CloseOnReplyHelper.onCompositionFinish();
    };

    const originalSendMessageLater = SendMessageLater;
    window.SendMessageLater = function(...args) {
      originalSendMessageLater(...args);
      CloseOnReplyHelper.onCompositionFinish();
    };

    document.documentElement.addEventListener('compose-window-init', this, false);
    document.documentElement.addEventListener('compose-window-close', this, false);
    window.addEventListener('unload', this, false);
  },

  handleEvent(aEvent) {
    switch (aEvent.type) {
      case 'compose-window-init':
        gMsgCompose.RegisterStateListener(this);
        return;

      case 'compose-window-close':
        gMsgCompose.UnregisterStateListener(this);
        return;

      case 'unload':
        document.documentElement.removeEventListener('compose-window-init', this, false);
        document.documentElement.removeEventListener('compose-window-close', this, false);
        window.removeEventListener('unload', this, false);
        return;
    }
  },

  get shouldCloseOnStartComposition() {
    try {
      const { Services } = Components.utils.import('resource://gre/modules/Services.jsm');
      return Services.prefs.getBoolPref('closeonreply.shouldCloseOnStartComposition');
    }
    catch(e) {
      return true;
    }
  },

  notifyReady() {
    const { Services } = Components.utils.import('resource://gre/modules/Services.jsm');
    Services.obs.notifyObservers(null, 'closeonreply:composeWindowReady', null);
  },

  onCompositionFinish() {
    if (!this.shouldCloseOnStartComposition)
      this.notifyReady();
  },

  // nsIMsgComposeStateListener
  NotifyComposeFieldsReady() {},
  NotifyComposeBodyReady() {
    if (!this.shouldCloseOnStartComposition)
      return;
    // do it after all fields are constructed completely.
    setTimeout(this.notifyReady, 0);
  },
  NotifyComposeBodyReadyReply() {
    if (!this.shouldCloseOnStartComposition)
      return;
    // do it after all fields are constructed completely.
    setTimeout(this.notifyReady, 0);
  },
  ComposeProcessDone() {
    this.onCompositionFinish();
  },
  SaveInFolderDone() {}
};
CloseOnReplyHelper.init();
