.PHONY: xpi

all: xpi

xpi:
	rm -f ./*.xpi
	zip -r -0 closeonreply.xpi chrome default chrome.manifest install.rdf -x '*/.*' >/dev/null 2>/dev/null

