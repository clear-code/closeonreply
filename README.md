# Close On Reply (mod)

This is a forked version of the addon [Close On Reply](https://addons.thunderbird.net/thunderbird/addon/close-on-reply/). Here is the list of changes:

 * Support Thunderbird 52.
 * The original message window (or tab) is closed aftera message composition window is completely initialized, because immediate closing of the original message may break generated forwarded message and its attachments.
 * You can set the original message window (or tab) to be closed after the composition window is closed (or saved as a draft). Go to the config editor (`about:config`) and set `closeonreply.shouldCloseOnStartComposition` to `false`. Otherwise the original message window (or tab) will be closed just after the composition window is initialized.

